﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace FTF
{
    public class observerUI : MonoBehaviour
    {

        public GameObject _NameText;
        public GameObject _NumText;
        // Use this for initialization
        void Start()
        {
#if FTF_OBSERVER

#else
            gameObject.SetActive(false);
#endif
        }

        void ObserverUIUpdate(int playerNum)
        {
            _NameText.GetComponent<Text>().text = RoomInfo.main.players[playerNum].instance.userName;
            _NumText.GetComponent<Text>().text = (playerNum+1).ToString() + " / " + RoomInfo.main.maxUser.ToString();
        }
    }
}
