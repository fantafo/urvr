using System;
using UnityEngine;

namespace VRStandardAssets.Utils
{
    // 씬 안에서 오브젝트와의 상호작용을 구현한다.
    // 어떤 오브젝트(Collider 포함)를 발견하기 위해 ray를 사용한다.
    // VRInteractiveItem는 다른 클래스에서도 사용할 수 있게 있게 Public.
    // 메인 카메라에 배치한다.
    // 특이점 : VRInteractiveItem 함수를 바로 호출
    public class VREyeRaycaster : MonoBehaviour
    {
        public event Action<RaycastHit> OnRaycasthit; // This event is called every frame that the user's gaze is over a collider.

        [SerializeField]
        private Transform m_Camera;
        [SerializeField]
        private LayerMask m_ExclusionLayers; // raycast에서 제외할 Layer.
        [SerializeField]
        private Reticle m_Reticle; // The reticle, if applicable.
        [SerializeField]
        private VRInput m_VrInput; // 현재 VRInteractiveItem에서 입력 기반 이벤트를 호출하는 데 사용.
        [SerializeField]
        private bool m_ShowDebugRay; // Optionally show the debug ray.
        [SerializeField]
        private float m_DebugRayLength = 5f; // Debug ray length.
        [SerializeField]
        private float m_DebugRayDuration = 1f; // How long the Debug ray will remain visible.
        [SerializeField]
        private float m_RayLength = 500f; // How far into the scene the ray is cast.


        private VRInteractiveItem m_CurrentInteractible; // 현재 바라보고 있는 오브젝트의 VRInteractiveItem 컴포넌트
        private VRInteractiveItem m_LastInteractible; //이전에 바라봤던 오브젝트


        // Utility for other classes to get the current interactive item
        // 현재 바라보고 있는 오브젝트 public 필드
        public VRInteractiveItem CurrentInteractible
        {
            get { return m_CurrentInteractible; }
        }

        // VRInput 이벤트 함수 연결
        // VRInput 은 현재 바라보는 오브젝트에서만 작동한다.
        private void OnEnable()
        {
            m_VrInput.OnClick += HandleClick; // 클릭 !
            m_VrInput.OnDoubleClick += HandleDoubleClick; //더블클릭 !
            m_VrInput.OnUp += HandleUp; // 누르고 뗀 후 순간!
            m_VrInput.OnDown += HandleDown; // 누를 때 순간!
        }

        // VRInput 이벤트 함수 해제
        private void OnDisable()
        {
            m_VrInput.OnClick -= HandleClick;
            m_VrInput.OnDoubleClick -= HandleDoubleClick;
            m_VrInput.OnUp -= HandleUp;
            m_VrInput.OnDown -= HandleDown;
        }

        private void Update()
        {
            EyeRaycast();
        }

        private void EyeRaycast()
        {
            // Show the debug ray if required
            // 디버깅용 레이 발사!
            if (m_ShowDebugRay)
            {
                Debug.DrawRay(m_Camera.position, m_Camera.forward * m_DebugRayLength, Color.blue, m_DebugRayDuration);
            }

            // Create a ray that points forwards from the camera.
            // 카메라 전방으로 레이 발사
            Ray ray = new Ray(m_Camera.position, m_Camera.forward);
            RaycastHit hit;

            // Do the raycast forweards to see if we hit an interactive item
            if (Physics.Raycast(ray, out hit, m_RayLength, ~m_ExclusionLayers))
            {
                VRInteractiveItem interactible = hit.collider.GetComponent<VRInteractiveItem>(); //attempt to get the VRInteractiveItem on the hit object
                m_CurrentInteractible = interactible; // 바라보는 현재 오브젝트의 VRInteractiveItem

                // If we hit an interactive item and it's not the same as the last interactive item, then call Over
                // 현재 오브젝트와 그 오브젝트가 이전 오브젝트와 다르면 Over를 호출.
                if (interactible && interactible != m_LastInteractible)
                    interactible.Over();

                // Deactive the last interactive item 
                // 지난 오브젝트 초기화
                if (interactible != m_LastInteractible)
                    DeactiveLastInteractible();

                m_LastInteractible = interactible;

                // Something was hit, set at the hit position.
                // Reticle 자리잡기
                if (m_Reticle)
                    m_Reticle.SetPosition(hit);

                // 다른 클래스에서 이벤트 호출로 사용 : hit 구조체 매개변수
                if (OnRaycasthit != null)
                    OnRaycasthit(hit);
            }
            else
            {
                // Nothing was hit, deactive the last interactive item.
                // 아무 것도 HIT 하지 않을 경우, 지난 오브젝트 초기화
                DeactiveLastInteractible();
                m_CurrentInteractible = null;

                // Position the reticle at default distance.
                // Reticle 자리 잡기
                if (m_Reticle)
                    m_Reticle.SetPosition();
            }
        }

        // 지난 오브젝트 초기화
        private void DeactiveLastInteractible()
        {
            if (m_LastInteractible == null)
                return;

            // 지난 오브젝트는 out 상태
            m_LastInteractible.Out();
            m_LastInteractible = null;
        }

        // VRInput OnUp -> 오브젝트 UP
        private void HandleUp()
        {
            if (m_CurrentInteractible != null)
                m_CurrentInteractible.Up();
        }

        // VRInput OnDown -> 오브젝트 Down
        private void HandleDown()
        {
            if (m_CurrentInteractible != null)
                m_CurrentInteractible.Down();
        }

        // VRInput OnClick -> 오브젝트 Click
        private void HandleClick()
        {
            if (m_CurrentInteractible != null)
                m_CurrentInteractible.Click();
        }

        // VRInput OnDoubleClick -> 오브젝트 DoubleClick
        private void HandleDoubleClick()
        {
            if (m_CurrentInteractible != null)
                m_CurrentInteractible.DoubleClick();

        }
    }
}