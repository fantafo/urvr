﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SFramework.TweenAction;
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using UnityEngine.UI;
#endif

public partial class STween
{
    #region only To
    public static STweenState color(Transform trans, Color to, float duration)
    {
        if (trans != null)
        {
            return color(trans.gameObject, to, duration);
        }
        return color((Material)null, to, duration);
    }
    public static STweenState color(GameObject go, Color to, float duration)
    {
        if (go != null)
        {
            Renderer renderer;
            if ((renderer = go.GetComponent<Renderer>()) != null)
            {
                return color(renderer.material, to, duration);
            }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
            Graphic graphic;
            if ((graphic = go.GetComponent<Graphic>()) != null)
            {
                return color(graphic, to, duration);
            }
#endif
        }
        return color((Material)null, to, duration);
    }

    public static STweenState color(Renderer renderer, Color to, float duration)
    {
        return color(renderer != null ? renderer.material : null, to, duration);
    }
    public static STweenState color(Material material, Color to, float duration)
    {
        return Play<ActionColorMaterial, Material, Color, Color>(material, to, duration);
    }
    public static STweenState color(MeshFilter filter, Color to, float duration)
    {
        return color((filter != null ? filter.mesh : null), to, duration);
    }
    public static STweenState color(Mesh mesh, Color to, float duration)
    {
        return Play<ActionColorVertexMesh, Mesh, Color, Color>(mesh, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState color(Graphic graphic, Color to, float duration)
    {
        return Play<ActionColorGraphic, Graphic, Color, Color>(graphic, to, duration);
    }
#endif
    #endregion

    #region From, To
    public static STweenState color(Transform trans, Color from, Color to, float duration)
    {
        if (trans != null)
        {
            return color(trans.gameObject, from, to, duration);
        }
        return color((Material)null, from, to, duration);
    }
    public static STweenState color(GameObject go, Color from, Color to, float duration)
    {
        if (go != null)
        {
            Renderer renderer;
            if ((renderer = go.GetComponent<Renderer>()) != null)
            {
                return color(renderer.material, from, to, duration);
            }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
            Graphic graphic;
            if ((graphic = go.GetComponent<Graphic>()) != null)
            {
                return color(graphic, from, to, duration);
            }
#endif
        }
        return color((Material)null, from, to, duration);
    }

    public static STweenState color(Renderer renderer, Color from, Color to, float duration)
    {
        return color(renderer != null ? renderer.material : null, from, to, duration);
    }
    public static STweenState color(Material material, Color from, Color to, float duration)
    {
        return Play<ActionColorMaterial, Material, Color, Color>(material, from, to, duration);
    }
    public static STweenState color(MeshFilter filter, Color from, Color to, float duration)
    {
        return color((filter != null ? filter.mesh : null), from, to, duration);
    }
    public static STweenState color(Mesh mesh, Color from, Color to, float duration)
    {
        return Play<ActionColorVertexMesh, Mesh, Color, Color>(mesh, from, to, duration);
    }
#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState color(Graphic graphic, Color from, Color to, float duration)
    {
        return Play<ActionColorGraphic, Graphic, Color, Color>(graphic, from, to, duration);
    }
#endif
    #endregion
}
