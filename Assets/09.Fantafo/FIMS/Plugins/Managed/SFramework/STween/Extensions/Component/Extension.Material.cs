﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenMaterialExtension
{
    private static readonly Action<Tuple<Material, int>, float> _changeFloatProp = __changeMaterialFloat;
    private static void __changeMaterialFloat(Tuple<Material, int> obj, float val) { obj.a.SetFloat(obj.b, val); }
    public static STweenState twnFloat(this Material obj, string name, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Tuple<Material, int>>, Tuple<Material, int>, float>(new Tuple<Material, int>(obj, Shader.PropertyToID(name)), from, to, duration, _changeFloatProp);
    }
}