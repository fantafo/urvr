﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenInputFieldExtension
{
    private static readonly Action<InputField, Color> _changeInputFieldCaretColor = __changeInputFieldCaretColor;
    private static void __changeInputFieldCaretColor(InputField inputField, Color val) { inputField.caretColor = val; }
    public static STweenState twnCaretColor(this InputField inputField, Color to, float duration)
    {
        return STween.Play<ActionColorObject<InputField>, InputField, Color>(inputField, inputField.caretColor, to, duration, _changeInputFieldCaretColor);
    }
    public static STweenState twnCaretColor(this InputField inputField, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<InputField>, InputField, Color>(inputField, from, to, duration, _changeInputFieldCaretColor);
    }

    private static readonly Action<InputField, Color> _changeInputFieldSelectionColor = __changeInputFieldSelectionColor;
    private static void __changeInputFieldSelectionColor(InputField inputField, Color val) { inputField.selectionColor = val; }
    public static STweenState twnSelectionColor(this InputField inputField, Color to, float duration)
    {
        return STween.Play<ActionColorObject<InputField>, InputField, Color>(inputField, inputField.selectionColor, to, duration, _changeInputFieldSelectionColor);
    }
    public static STweenState twnSelectionColor(this InputField inputField, Color from, Color to, float duration)
    {
        return STween.Play<ActionColorObject<InputField>, InputField, Color>(inputField, from, to, duration, _changeInputFieldSelectionColor);
    }


    private static readonly Action<InputField, float> _changeInputFieldCaretBlinkRate = __changeInputFieldCaretBlinkRate;
    private static void __changeInputFieldCaretBlinkRate(InputField inputField, float val) { inputField.caretBlinkRate = val; }
    public static STweenState twnCaretBlinkRate(this InputField inputField, float to, float duration)
    {
        return STween.Play<ActionFloatObject<InputField>, InputField, float>(inputField, inputField.caretBlinkRate, to, duration, _changeInputFieldCaretBlinkRate);
    }
    public static STweenState twnCaretBlinkRate(this InputField inputField, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<InputField>, InputField, float>(inputField, from, to, duration, _changeInputFieldCaretBlinkRate);
    }


    private static readonly Action<InputField, int> _changeInputFieldCaretPosition = __changeInputFieldCaretPosition;
    private static void __changeInputFieldCaretPosition(InputField inputField, int val) { inputField.caretPosition = val; }
    public static STweenState twnCaretPosition(this InputField inputField, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, inputField.caretPosition, to, duration, _changeInputFieldCaretPosition);
    }
    public static STweenState twnCaretPosition(this InputField inputField, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, from, to, duration, _changeInputFieldCaretPosition);
    }


    private static readonly Action<InputField, int> _changeInputFieldCaretWidth = __changeInputFieldCaretWidth;
    private static void __changeInputFieldCaretWidth(InputField inputField, int val) { inputField.caretWidth = val; }
    public static STweenState twnCaretWidth(this InputField inputField, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, inputField.caretWidth, to, duration, _changeInputFieldCaretWidth);
    }
    public static STweenState twnCaretWidth(this InputField inputField, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, from, to, duration, _changeInputFieldCaretWidth);
    }


    private static readonly Action<InputField, int> _changeInputFieldCharacterLimit = __changeInputFieldCharacterLimit;
    private static void __changeInputFieldCharacterLimit(InputField inputField, int val) { inputField.characterLimit = val; }
    public static STweenState twnCharacterLimit(this InputField inputField, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, inputField.characterLimit, to, duration, _changeInputFieldCharacterLimit);
    }
    public static STweenState twnCharacterLimit(this InputField inputField, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, from, to, duration, _changeInputFieldCharacterLimit);
    }


    private static readonly Action<InputField, int> _changeInputFieldSelectionAnchorPosition = __changeInputFieldSelectionAnchorPosition;
    private static void __changeInputFieldSelectionAnchorPosition(InputField inputField, int val) { inputField.selectionAnchorPosition = val; }
    public static STweenState twnSelectionAnchorPosition(this InputField inputField, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, inputField.selectionAnchorPosition, to, duration, _changeInputFieldSelectionAnchorPosition);
    }
    public static STweenState twnSelectionAnchorPosition(this InputField inputField, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, from, to, duration, _changeInputFieldSelectionAnchorPosition);
    }


    private static readonly Action<InputField, int> _changeInputFieldSelectionFocusPosition = __changeInputFieldSelectionFocusPosition;
    private static void __changeInputFieldSelectionFocusPosition(InputField inputField, int val) { inputField.selectionFocusPosition = val; }
    public static STweenState twnSelectionFocusPosition(this InputField inputField, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, inputField.selectionFocusPosition, to, duration, _changeInputFieldSelectionFocusPosition);
    }
    public static STweenState twnSelectionFocusPosition(this InputField inputField, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<InputField>, InputField, int>(inputField, from, to, duration, _changeInputFieldSelectionFocusPosition);
    }



}
#endif