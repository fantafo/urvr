﻿using UnityEngine;

public static class STweenGameObjectExtension
{
    #region only To
    public static STweenState twnMove(this GameObject go, Vector3 to, float duration)
    {
        return STween.move(go.transform, to, duration);
    }
    public static STweenState twnMoveLocal(this GameObject go, Vector3 to, float duration)
    {
        return STween.moveLocal(go.transform, to, duration);
    }
    public static STweenState twnMoveX(this GameObject go, float to, float duration)
    {
        return STween.moveX(go.transform, to, duration);
    }
    public static STweenState twnMoveY(this GameObject go, float to, float duration)
    {
        return STween.moveY(go.transform, to, duration);
    }
    public static STweenState twnMoveZ(this GameObject go, float to, float duration)
    {
        return STween.moveZ(go.transform, to, duration);
    }
    public static STweenState twnMoveLocalX(this GameObject go, float to, float duration)
    {
        return STween.moveLocalX(go.transform, to, duration);
    }
    public static STweenState twnMoveLocalY(this GameObject go, float to, float duration)
    {
        return STween.moveLocalY(go.transform, to, duration);
    }
    public static STweenState twnMoveLocalZ(this GameObject go, float to, float duration)
    {
        return STween.moveLocalZ(go.transform, to, duration);
    }

    public static STweenState twnRotate(this GameObject comp, Vector3 to, float duration)
    {
        return STween.rotate(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocal(this GameObject comp, Vector3 to, float duration)
    {
        return STween.rotateLocal(comp.transform, to, duration);
    }
    public static STweenState twnRotate(this GameObject comp, Quaternion to, float duration)
    {
        return STween.rotate(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocal(this GameObject comp, Quaternion to, float duration)
    {
        return STween.rotateLocal(comp.transform, to, duration);
    }
    public static STweenState twnRotateQ(this GameObject comp, Vector3 to, float duration)
    {
        return STween.rotateQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalQ(this GameObject comp, Vector3 to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateQ(this GameObject comp, Quaternion to, float duration)
    {
        return STween.rotateQ(comp.transform, to, duration);
    }
    public static STweenState twnRotateLocalQ(this GameObject comp, Quaternion to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, to, duration);
    }

    public static STweenState twnRotateX(this GameObject go, float to, float duration)
    {
        return STween.rotateX(go.transform, to, duration);
    }
    public static STweenState twnRotateY(this GameObject go, float to, float duration)
    {
        return STween.rotateY(go.transform, to, duration);
    }
    public static STweenState twnRotateZ(this GameObject go, float to, float duration)
    {
        return STween.rotateZ(go.transform, to, duration);
    }
    public static STweenState twnRotateLocalX(this GameObject go, float to, float duration)
    {
        return STween.rotateLocalX(go.transform, to, duration);
    }
    public static STweenState twnRotateLocalY(this GameObject go, float to, float duration)
    {
        return STween.rotateLocalY(go.transform, to, duration);
    }
    public static STweenState twnRotateLocalZ(this GameObject go, float to, float duration)
    {
        return STween.rotateLocalZ(go.transform, to, duration);
    }

    public static STweenState twnScale(this GameObject go, Vector3 to, float duration)
    {
        return STween.scale(go.transform, to, duration);
    }
    public static STweenState twnScaleX(this GameObject go, float to, float duration)
    {
        return STween.scaleX(go.transform, to, duration);
    }
    public static STweenState twnScaleY(this GameObject go, float to, float duration)
    {
        return STween.scaleY(go.transform, to, duration);
    }
    public static STweenState twnScaleZ(this GameObject go, float to, float duration)
    {
        return STween.scaleZ(go.transform, to, duration);
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnSize(this GameObject go, Vector2 to, float duration)
    {
        return STween.size(go.transform as RectTransform, to, duration);
    }
    public static STweenState twnSizeX(this GameObject go, float to, float duration)
    {
        return STween.sizeX(go.transform as RectTransform, to, duration);
    }
    public static STweenState twnSizeY(this GameObject go, float to, float duration)
    {
        return STween.sizeY(go.transform as RectTransform, to, duration);
    }

    public static STweenState twnAnchored(this GameObject go, Vector3 to, float duration)
    {
        return STween.anchored(go.transform as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredX(this GameObject go, float to, float duration)
    {
        return STween.anchoredX(go.transform as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredY(this GameObject go, float to, float duration)
    {
        return STween.anchoredY(go.transform as RectTransform, to, duration);
    }
    public static STweenState twnAnchoredZ(this GameObject go, float to, float duration)
    {
        return STween.anchoredZ(go.transform as RectTransform, to, duration);
    }
#endif

    public static STweenState twnAlpha(this GameObject go, float to, float duration)
    {
        return STween.alpha(go.transform, to, duration);
    }
    public static STweenState twnColor(this GameObject go, Color to, float duration)
    {
        return STween.color(go.transform, to, duration);
    }
    #endregion

    #region From, To
    public static STweenState twnMove(this GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return STween.move(go.transform, from, to, duration);
    }
    public static STweenState twnMoveLocal(this GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return STween.moveLocal(go.transform, from, to, duration);
    }
    public static STweenState twnMoveX(this GameObject go, float from, float to, float duration)
    {
        return STween.moveX(go.transform, from, to, duration);
    }
    public static STweenState twnMoveY(this GameObject go, float from, float to, float duration)
    {
        return STween.moveY(go.transform, from, to, duration);
    }
    public static STweenState twnMoveZ(this GameObject go, float from, float to, float duration)
    {
        return STween.moveZ(go.transform, from, to, duration);
    }
    public static STweenState twnMoveLocalX(this GameObject go, float from, float to, float duration)
    {
        return STween.moveLocalX(go.transform, from, to, duration);
    }
    public static STweenState twnMoveLocalY(this GameObject go, float from, float to, float duration)
    {
        return STween.moveLocalY(go.transform, from, to, duration);
    }
    public static STweenState twnMoveLocalZ(this GameObject go, float from, float to, float duration)
    {
        return STween.moveLocalZ(go.transform, from, to, duration);
    }

    public static STweenState twnRotate(this GameObject comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotate(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocal(this GameObject comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnRotate(this GameObject comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotate(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocal(this GameObject comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateLocal(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateQ(this GameObject comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalQ(this GameObject comp, Vector3 from, Vector3 to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateQ(this GameObject comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateQ(comp.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalQ(this GameObject comp, Quaternion from, Quaternion to, float duration)
    {
        return STween.rotateLocalQ(comp.transform, from, to, duration);
    }


    public static STweenState twnRotateX(this GameObject go, float from, float to, float duration)
    {
        return STween.rotateX(go.transform, from, to, duration);
    }
    public static STweenState twnRotateY(this GameObject go, float from, float to, float duration)
    {
        return STween.rotateY(go.transform, from, to, duration);
    }
    public static STweenState twnRotateZ(this GameObject go, float from, float to, float duration)
    {
        return STween.rotateZ(go.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalX(this GameObject go, float from, float to, float duration)
    {
        return STween.rotateLocalX(go.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalY(this GameObject go, float from, float to, float duration)
    {
        return STween.rotateLocalY(go.transform, from, to, duration);
    }
    public static STweenState twnRotateLocalZ(this GameObject go, float from, float to, float duration)
    {
        return STween.rotateLocalZ(go.transform, from, to, duration);
    }

    public static STweenState twnScale(this GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return STween.scale(go.transform, from, to, duration);
    }

    public static STweenState twnScaleX(this GameObject go, float from, float to, float duration)
    {
        return STween.scaleX(go.transform, from, to, duration);
    }
    public static STweenState twnScaleY(this GameObject go, float from, float to, float duration)
    {
        return STween.scaleY(go.transform, from, to, duration);
    }
    public static STweenState twnScaleZ(this GameObject go, float from, float to, float duration)
    {
        return STween.scaleZ(go.transform, from, to, duration);
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    public static STweenState twnSize(this GameObject go, Vector2 from, Vector2 to, float duration)
    {
        return STween.size(go.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnSizeX(this GameObject go, float from, float to, float duration)
    {
        return STween.sizeX(go.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnSizeY(this GameObject go, float from, float to, float duration)
    {
        return STween.sizeY(go.transform as RectTransform, from, to, duration);
    }

    public static STweenState twnAnchored(this GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return STween.anchored(go.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredX(this GameObject go, float from, float to, float duration)
    {
        return STween.anchoredX(go.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredY(this GameObject go, float from, float to, float duration)
    {
        return STween.anchoredY(go.transform as RectTransform, from, to, duration);
    }
    public static STweenState twnAnchoredZ(this GameObject go, float from, float to, float duration)
    {
        return STween.anchoredZ(go.transform as RectTransform, from, to, duration);
    }
#endif

    public static STweenState twnAlpha(this GameObject go, float from, float to, float duration)
    {
        return STween.alpha(go.transform, from, to, duration);
    }
    public static STweenState twnColor(this GameObject go, Color from, Color to, float duration)
    {
        return STween.color(go.transform, from, to, duration);
    }
    #endregion
}