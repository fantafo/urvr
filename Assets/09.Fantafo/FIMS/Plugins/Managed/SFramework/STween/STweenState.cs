﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using SFramework.TweenAction;
using UnityEngine;
using SFramework;
using UObject = UnityEngine.Object;
using System.Collections;

public enum SLoopType : byte
{
    Once,
    Clamp,
    PingPong
}

[Flags]
public enum STweenReleasable : byte
{
    None = 0,
    State = 1,
    Action = 2,

    All = (State | Action)
}

[Serializable]
public class STweenState : IObjectLinkedListNode<STweenState>
{
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Variables                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

#if STWEEN_DEBUG || UNITY_EDITOR
    public string name;
    internal string callStack;
#endif

    static int generateID = 0;
    public int instanceID = ++generateID;

    // flags
    internal STweenReleasable _releasable = STweenReleasable.All;
    public bool _lateUpdate;
    public bool _beginApply;
    public string _tag;
    public object _ref;
    public int _playId;

    // time variables
    public bool _unscaledTime;

    public float _duration;
    public float _delay;
    internal float _startTime;
    internal float _pauseTime;

    // tween direction
    internal bool _forward = true;

    // loop
    public SLoopType _loopType;
    public int _loopCount;

    // actions
    public BaseTweenAction _action;

    // check object
    public Behaviour _checkBehaviour; // (Slow) check Component.isActiveAndEnabled
    public GameObject _checkGameObject; // (Slow) check GameObject.activeInHierarchy

    // easing
    public bool _easeReverse; // process pingpong. reverse Curve and Easing
    public float _easePow = 1; // proc Easing Mathf.Pow

    internal Func<float, float> _easeFunc; // check Second
    public SEasingType _easeType; // check Last
    public AnimationCurve _easeCurve;

    public override string ToString()
    {
        string tostring = string.Format("{0}(InstID:{8}/PlayId:{1}/Action:{2}/Dur:{3}/Loop:{4}({5})/ease:{6}/Release:{7})", _tag ?? "State", _playId, _action, _duration, _loopType, _loopCount, _easeType, _releasable, instanceID);
#if UNITY_EDITOR
        tostring += "\r\n"+name+"\r\nStackTrace\r\n" + callStack + "\r\n-------------------------\r\n";
#endif
        return tostring;
    }

    // events
    internal event Action onStart;
    internal event Action onUpdate;
    internal event Action onComplete;
    internal event Action onCancel;
    internal event Func<bool> onUpdateFunc;

    internal object _startArg;
    internal object _updateArg;
    internal object _completeArg;
    internal event Action<object> onStartObj;
    internal event Action<object> onUpdateObj;
    internal event Action<object> onCompleteObj;

    internal ITweenCallback _delegater;

    // cache
    internal bool _initialized;
    internal bool _started;
    internal bool _completed;
    internal bool _pause;
    internal int __procLoopCount;
    private object __signiture;

    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                          Properties                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static implicit operator bool(STweenState state)
    {
        return state != null && state.__signiture == STween.CONTAINER_SIGNITURE;
    }

    public bool IsPlaying { get { return !_pause && !_completed; } }
    public bool IsPause { get { return _pause; } }

    public object parent { get; set; }
    public STweenState Next { get; set; }
    public STweenState Prev { get; set; }

    public bool EnableBehaviour
    {
        get
        {
#if UNITY_5
            return _checkBehaviour.isActiveAndEnabled;
#else
			return _checkBehaviour.enabled;
#endif
        }
    }
    public bool EnableGameObject
    {
        get
        {
            return _checkGameObject.activeInHierarchy;
        }
    }


    public string Tag { get { return _tag; } set { _tag = value; } }
    public object Ref { get { return _ref; } set { _ref = value; } }

    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                    Internal Life Cycles                     //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    /// <summary>
    /// prepare start tween
    /// </summary>
    /// <param name="time">Current Play Time</param>
    internal void Prepare(float time)
    {
        if (Tag == "imm")
        {
            SLog.Debug("imm", instanceID + " / " + _playId + " // Prepare " + time + " + " + _delay);
        }
        _startTime = time + _delay;
        __procLoopCount = _loopCount;
        _initialized = true;
        __signiture = STween.CONTAINER_SIGNITURE;

        if (_beginApply)
            _action.Update(0);
    }

    public void AddPlayId()
    {
        _playId++;
    }

    public void ClearRunable()
    {
        _initialized = false;
        _started = false;
        _completed = false;
        _pause = false;
        _forward = true;
    }

    /// <summary>
    /// Clear All Data. call is Not before tween.
    /// </summary>
    internal void Clear()
    {
        _releasable = STweenReleasable.All;

        ClearRunable();
        _lateUpdate = false;
        _beginApply = false;
        _tag = null;
        _ref = null;

        _unscaledTime = false;

        _duration = 0;
        _delay = 0;
        _startTime = 0;
        _pauseTime = 0;

        _loopType = SLoopType.Once;
        _loopCount = 1;

        _action = null;

        _checkGameObject = null;
        _checkBehaviour = null;

        _easeReverse = false;
        _easePow = 1;
        _easeFunc = null;
        _easeCurve = null;
        _easeType = SEasingType.linear;

        onStart = null;
        onUpdate = null;
        onUpdateFunc = null;
        onComplete = null;
        onCancel = null;

        onStartObj = null;
        onUpdateObj = null;
        onCompleteObj = null;

        _startArg = null;
        _updateArg = null;
        _completeArg = null;

        _delegater = null;

#if UNITY_EDITOR || STWEEN_DEBUG
        name = null;
#endif
    }

    public STweenState Set(
        string tag = null,
        object r = null,
        float delay = 0,
        bool beginApply = false,
        int loop = 1,
        SLoopType loopType = SLoopType.Once,
        UObject check = null,
        Func<float, float> easeFunc = null,
        SEasingType ease = SEasingType.linear,
        AnimationCurve easeCurve = null,
        Action start = null,
        Action update = null,
        Action complete = null,
        Action cancel = null,
        Action<object> startObj = null,
        Action<object> updateObj = null,
        Action<object> completeObj = null,
        object startArg = null,
        object updateArg = null,
        object completeArg = null,
        ITweenCallback delegater = null)
    {
        _tag = tag;
        _ref = r;
        _delay = delay;
        _beginApply = beginApply;

        _loopCount = loop;
        _loopType = loopType;

        if (check is GameObject)
            _checkGameObject = (GameObject)check;
        else if (check is Behaviour)
            _checkBehaviour = (Behaviour)check;

        if (easeFunc != null)
        {
            _easeFunc = easeFunc;
        }
        else if (easeCurve != null)
        {
            _easeType = SEasingType.animationCurve;
            _easeCurve = easeCurve;
        }
        else
        {
            _easeType = ease;
        }

        onStart = start;
        onUpdate = update;
        onComplete = complete;
        onCancel = cancel;

        onStartObj = startObj;
        onUpdateObj = updateObj;
        onCompleteObj = completeObj;

        _startArg = startArg;
        _updateArg = updateArg;
        _completeArg = completeArg;

        _delegater = delegater;

        return this;
    }


    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                      Life Cycles Control                    //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    /// <summary>
    /// Play tween. attach Running, release Pause, Replay
    /// (only DontRelease() State)
    /// (If the pool is dangerous if the Registed.)
    /// </summary>
    public void Play()
    {
        if (_initialized)
        {
            if (_completed)
            {
                Replay();
                return;
            }
            else if (_pause)
            {
                STween.Resume(this);
                return;
            }
        }
        STween.Attach(this);
    }

    /// <summary>
    /// Replay Tween. 
    /// (only DontRelease() State)
    /// (If the pool is dangerous if the Registed.)
    /// </summary>
    public void Replay()
    {
        if (_initialized)
        {
            if (_completed)
            {
                STween.Attach(this);
            }

            _initialized = false;
            _started = false;
            _pause = false;
            _completed = false;
            _forward = true;
        }
    }

    /// <summary>
    /// Pause Tween
    /// </summary>
    public void Pause()
    {
        STween.Pause(this);
    }

    /// <summary>
    /// Stop Tween
    /// </summary>
    public void Stop(bool callOnComplete = false)
    {
        STween.Stop(this, callOnComplete);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                       Execute Events                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    internal void ExecuteOnStart()
    {
        _started = true;
        if (onStart != null)
            onStart();
        if (onStartObj != null)
            onStartObj(_startArg);
        if (_delegater is ITweenStart)
            ((ITweenStart)_delegater).OnTweenStart(this);
    }
    internal bool ExecuteOnUpdate()
    {
        if (onUpdate != null)
            onUpdate();
        if (onUpdateObj != null)
            onUpdateObj(_updateArg);
        if (onUpdateFunc != null)
            return onUpdateFunc();
        if (_delegater is ITweenUpdate)
            ((ITweenUpdate)_delegater).OnTweenUpdate(this);
        return true;
    }
    internal void ExecuteOnComplete()
    {
        _completed = true;
        if (onComplete != null)
            onComplete();
        if (onCompleteObj != null)
            onCompleteObj(_completeArg);
        if (_delegater is ITweenComplete)
            ((ITweenComplete)_delegater).OnTweenComplete(this);
    }
    internal void ExecuteOnCancel()
    {
        _completed = true;
        if (onCancel != null)
            onCancel();
        if (_delegater is ITweenCancel)
            ((ITweenCancel)_delegater).OnTweenCancel(this);
    }
    public void UpdateNow(float ratio = 0)
    {
        if (_action != null)
        {
            _action.Update(ratio);
        }
    }


    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Variable Setup                      //
    //                                                             //
    /////////////////////////////////////////////////////////////////


#if STWEEN_DEBUG
    public STweenState SetName(string name)
    {
        this.name = name;
        return this;
    }
#endif

    /// <summary>
    /// Setup Tag
    /// </summary>
    public STweenState SetTag(string tag)
    {
        _tag = tag;
        return this;
    }

    /// <summary>
    /// Setup Ref
    /// </summary>
    public STweenState SetRef(object r)
    {
        _ref = r;
        return this;
    }

    /// <summary>
    /// Dont Release Pools
    /// </summary>
    public STweenState DontRelease(STweenReleasable releasable = STweenReleasable.All)
    {
        _releasable = STweenReleasable.All ^ releasable;
        return this;
    }

    /// <summary>
    /// move to lateUpdate
    /// </summary>
    public STweenState UseLateUpdate()
    {
        _lateUpdate = true;
        return this;
    }

    /// <summary>
    /// Delay before applying the 0 ratio on the run.
    /// </summary>
    public STweenState BeginApplying()
    {
        _beginApply = true;
        return this;
    }


    /// <summary>
    /// use Tween For Unscaled Time 
    /// </summary>
    /// <returns></returns>
    public STweenState UseUnscaledTime()
    {
        _unscaledTime = true;
        return this;
    }

    /// <summary>
    /// set tween duration
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public STweenState Duration(float value)
    {
        _duration = value;
        return this;
    }

    /// <summary>
    /// set delay start time
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public STweenState Delay(float value)
    {
        _delay = value;
        return this;
    }


    /// <summary>
    /// loop tween
    /// </summary>
    /// <param name="count">'0' is Play Eternal</param>
    public STweenState Loop(SLoopType type = SLoopType.Clamp, int count = 0)
    {
        _loopType = type;
        _loopCount = count;
        return this;
    }
    /// <summary>
    /// loop pingpong
    /// </summary>
    /// <param name="count">'0' is Play Eternal</param>
    public STweenState PingPong(int count = 0)
    {
        _loopType = SLoopType.PingPong;
        _loopCount = count;
        return this;
    }
    /// <summary>
    /// loop clamp.
    /// </summary>
    /// <param name="count">'0' is Play Eternal</param>
    public STweenState Clamp(int count = 0)
    {
        _loopType = SLoopType.Clamp;
        _loopCount = count;
        return this;
    }


    /// <summary>
    /// Set Action and Action.parent
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public STweenState SetAction(BaseTweenAction action)
    {
        _action = action;
        action._state = this;
#if STWEEN_DEBUG || UNITY_EDITOR
        name = (action.Target is UnityEngine.Object) ? ((UnityEngine.Object)action.Target).name : name;
#endif
        return this;
    }


    /// <summary>
    /// Enable Check. If inactive GameObject 
    /// </summary>
    public STweenState Check(GameObject go)
    {
        _checkGameObject = go;
        return this;
    }
    /// <summary>
    /// Enable Check. If inactive Behaviour 
    /// </summary>
    public STweenState Check(Behaviour comp)
    {
        _checkBehaviour = comp;
        return this;
    }

    /// <summary>
    /// easing a reverse work
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public STweenState EaseReverse()
    {
        _easeReverse = true;
        return this;
    }
    /// <summary>
    /// Amplifying the easing value. 
    /// </summary>
    public STweenState EasePow(float pow)
    {
        _easePow = pow;
        return this;
    }

    /// <summary>
    /// set primitive easing type
    /// </summary>
    public STweenState Ease(SEasingType type)
    {
        _easeType = type;
        return this;
    }
    /// <summary>
    /// set easing from AnimationCurve (0.0 ~ 1.0 time key)
    /// </summary>
    public STweenState Ease(AnimationCurve curve)
    {
        _easeCurve = curve;
        _easeType = SEasingType.animationCurve;
        return this;
    }
    /// <summary>
    /// set easing from value to return in function
    /// </summary>
    public STweenState Ease(Func<float, float> func)
    {
        _easeFunc = func;
        return this;
    }


    /// <summary>
    /// Set Action From.
    /// </summary>
    /// <code>
    /// like : 
    /// tween.From(Vector3.zero); // skip Generic Type
    /// tween.From<Vector3>(Vector3.zero);
    /// 
    /// tween.From(1f); // skip Generic Type
    /// tween.From<float>(1);
    /// 
    /// tween.From(1);
    /// tween.From<int>(1);
    /// </code>
    /// <typeparam name="T"></typeparam>
    /// <param name="from"></param>
    /// <returns></returns>
    public STweenState From<T>(T from)
    {
        if (_action is VariableAction<T>)
        {
            ((VariableAction<T>)_action).SetFrom(from);
        }
        else
        {
            Debug.LogError("Not Match Variable Type " + _action.GetType().Name + " to " + from.GetType().Name);
        }
        return this;
    }

    /// <summary>
    /// Set Action To.
    /// <code>
    /// * It's same both the code.
    /// tween.To(Vector3.one); // skip Generic Type
    /// tween.To<Vector3>(Vector3.zero);
    /// 
    /// tween.To(1f); // skip Generic Type
    /// tween.To<float>(1);
    /// 
    /// tween.To(1);
    /// tween.To<int>(1);
    /// </code>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="from"></param>
    /// <returns></returns>
    public STweenState To<T>(T from)
    {
        if (_action is VariableAction<T>)
        {
            ((VariableAction<T>)_action).SetTo(from);
        }
        else
        {
            Debug.LogError("Not Match Variable Type " + _action.GetType().Name + " to " + from.GetType().Name);
        }
        return this;
    }



    public STweenState SetOnStart(Action action)
    {
        onStart = action;
        return this;
    }
    public STweenState SetOnUpdate(Action action)
    {
        onUpdate = action;
        return this;
    }
    public STweenState SetOnUpdate(Func<bool> action)
    {
        onUpdateFunc = action;
        return this;
    }
    public STweenState SetOnComplete(Action action)
    {
        onComplete = action;
        return this;
    }
    public STweenState SetOnCancel(Action action)
    {
        onCancel = action;
        return this;
    }
    public STweenState SetOnStart(Action<object> action, object obj = null)
    {
        onStartObj = action;
        _startArg = obj ?? this;
        return this;
    }
    public STweenState SetOnUpdate(Action<object> action, object obj = null)
    {
        onUpdateObj = action;
        _updateArg = obj ?? this;
        return this;
    }
    public STweenState SetOnComplete(Action<object> action, object obj = null)
    {
        onCompleteObj = action;
        _completeArg = obj ?? this;
        return this;
    }

    public STweenState SetDelegater(ITweenCallback delegater)
    {
        _delegater = delegater;
        return this;
    }


    public IEnumerator Wait()
    {
        int playID = this._playId;
        while (playID == this._playId)
        {
            yield return null;
        }
    }
}

public static class __STweenStateExtension
{
    public static void TryStop(this STweenState state)
    {
        if (state != null)
        {
            state.Stop();
        }
    }
    public static void TryStop(this STweenState state, bool callOnComplete)
    {
        if (state != null)
        {
            state.Stop(callOnComplete);
        }
    }
}