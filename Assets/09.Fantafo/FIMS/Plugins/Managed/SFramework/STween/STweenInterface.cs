﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface ITweenCallback { }

public interface ITweenStart : ITweenCallback
{
    void OnTweenStart(STweenState state);
}
public interface ITweenUpdate : ITweenCallback
{
    void OnTweenUpdate(STweenState state);
}
public interface ITweenComplete : ITweenCallback
{
    void OnTweenComplete(STweenState state);
}
public interface ITweenCancel : ITweenCallback
{
    void OnTweenCancel(STweenState state);
}

public struct TweenActivator : ITweenComplete
{
    private GameObject obj;
    private bool active;

    public TweenActivator(GameObject obj, bool active)
    {
        this.obj = obj;
        this.active = active;
    }

    public void OnTweenComplete(STweenState state)
    {
        obj.SetActive(active);
    }
}