﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace ExtensionEditor
{
    public class Aligner : BaseExtensionEditor
    {

        [MenuItem("GameObject/Align/Align Position")]
        public static void AlignAll()
        {
            AlignX();
            AlignY();
            AlignZ();
        }

        [MenuItem("GameObject/Align/Align X %#x")]
        public static void AlignX()
        {
            if (init("Align X", 1))
            {
                float min = MaxValue(t => t.position.x);
                Loop(t => t.position = new Vector3(min, t.position.y, t.position.z));
            }
        }

        [MenuItem("GameObject/Align/Align Y %#y")]
        public static void AlignY()
        {
            if (init("Align Y", 1))
            {
                float min = MinValue(t => t.position.y);
                Loop(t => t.position = new Vector3(t.position.x, min, t.position.z));
            }
        }

        [MenuItem("GameObject/Align/Align Z %#z")]
        public static void AlignZ()
        {
            if (init("Align Z", 1))
            {
                float min = MaxValue(t => t.position.z);
                Loop(t => t.position = new Vector3(t.position.x, t.position.y, min));
            }
        }

        [MenuItem("GameObject/Align/Distribute Position")]
        public static void DistributeAll()
        {
            DistributeX();
            DistributeY();
            DistributeZ();
        }
        [MenuItem("GameObject/Align/Distribute X %&#x")]
        public static void DistributeX()
        {
            if (init("Distribute X", 2))
            {
                Sort(t => t.position.x);
                float min = MinValue(t => t.position.x);
                float max = MaxValue(t => t.position.x);

                int i = 0;
                Loop(t =>
                {
                    float value = (max - min) / (transforms.Length - 1);
                    value *= i++;
                    value += min;
                    t.position = new Vector3(value, t.position.y, t.position.z);
                });
            }
        }

        [MenuItem("GameObject/Align/Distribute Y %&#y")]
        public static void DistributeY()
        {
            if (init("Distribute Y", 2))
            {
                Sort(t => t.position.y);
                float min = MinValue(t => t.position.y);
                float max = MaxValue(t => t.position.y);

                int i = 0;
                Loop(t =>
                {
                    float value = (max - min) / (transforms.Length - 1);
                    value *= i++;
                    value += min;
                    t.position = new Vector3(t.position.x, value, t.position.z);
                });
            }
        }

        [MenuItem("GameObject/Align/Distribute Z %&#z")]
        public static void DistributeZ()
        {
            if (init("Distribute Z", 2))
            {
                Sort(t => t.position.z);
                float min = MinValue(t => t.position.z);
                float max = MaxValue(t => t.position.z);

                int i = 0;
                Loop(t =>
                {
                    float value = (max - min) / (transforms.Length - 1);
                    value *= i++;
                    value += min;
                    t.position = new Vector3(t.position.x, t.position.y, value);
                });
            }
        }

        [MenuItem("GameObject/Align/By Hierarchy/Distribute Position")]
        public static void DistributeAllByHierarchy()
        {
            DistributeXByHierarchy();
            DistributeYByHierarchy();
            DistributeZByHierarchy();
        }
        [MenuItem("GameObject/Align/By Hierarchy/Distribute X")]
        public static void DistributeXByHierarchy()
        {
            if (init("Distribute X ByHierarchy", 2))
            {
                Sort(t => t.GetSiblingIndex());
                float min = MinValue(t => t.position.x);
                float max = MaxValue(t => t.position.x);

                int i = 0;
                Loop(t =>
                {
                    float value = (max - min) / (transforms.Length - 1);
                    value *= i++;
                    value += min;
                    t.position = new Vector3(value, t.position.y, t.position.z);
                });
            }
        }

        [MenuItem("GameObject/Align/By Hierarchy/Distribute Y")]
        public static void DistributeYByHierarchy()
        {
            if (init("Distribute Y ByHierarchy", 2))
            {
                Sort(t => t.GetSiblingIndex());
                float min = MinValue(t => t.position.y);
                float max = MaxValue(t => t.position.y);

                int i = 0;
                Loop(t =>
                {
                    float value = (max - min) / (transforms.Length - 1);
                    value *= i++;
                    value += min;
                    t.position = new Vector3(t.position.x, value, t.position.z);
                });
            }
        }

        [MenuItem("GameObject/Align/By Hierarchy/Distribute Z")]
        public static void DistributeZByHierarchy()
        {
            if (init("Distribute Z ByHierarchy", 2))
            {
                Sort(t => t.GetSiblingIndex());
                float min = MinValue(t => t.position.z);
                float max = MaxValue(t => t.position.z);

                int i = 0;
                Loop(t =>
                {
                    float value = (max - min) / (transforms.Length - 1);
                    value *= i++;
                    value += min;
                    t.position = new Vector3(t.position.x, t.position.y, value);
                });
            }
        }
    }
}