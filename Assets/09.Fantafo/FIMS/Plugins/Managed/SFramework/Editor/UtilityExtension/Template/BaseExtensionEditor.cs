﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class BaseExtensionEditor
    {
        protected static Transform[] transforms;

        protected static bool init(string name, int length)
        {
            transforms = Selection.transforms;
            Undo.RecordObjects(transforms, name);
            return transforms != null && transforms.Length > length;
        }

        protected static void Loop(Action<Transform> act)
        {
            foreach (Transform trans in transforms)
            {
                act(trans);
            }
        }

        protected static void Sort(Func<Transform, float> act)
        {
            Array.Sort<Transform>(transforms, (l, r) =>
            {
                return act(l).CompareTo(act(r));
            });
        }

        protected static float MinValue(Func<Transform, float> act)
        {
            float min = float.MaxValue;
            foreach (Transform trans in transforms)
            {
                float v = act(trans);
                if (v < min)
                {
                    min = v;
                }
            }
            return min;
        }

        protected static float MaxValue(Func<Transform, float> act)
        {
            float max = float.MinValue;
            foreach (Transform trans in transforms)
            {
                float v = act(trans);
                if (v > max)
                {
                    max = v;
                }
            }
            return max;
        }

    }
}