﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Sions
{
    public class SkinnedMeshRetargeter : EditorWindow
    {
        [MenuItem("SF/Tools/Skinned Mesh Retargeter")]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<SkinnedMeshRetargeter>();
        }

        GameObject _from;
        GameObject[] _to = new GameObject[0];

        void OnGUI()
        {
            EditorGUIUtility.labelWidth = 70;

            _from = (GameObject)EditorGUILayout.ObjectField("Root", _from, typeof(GameObject), true);

            for (int i = 0; i < _to.Length; i++)
            {
                _to[i] = (GameObject)EditorGUILayout.ObjectField("Include", _to[i], typeof(GameObject), true);
                if (!_to[i])
                {
                    _to = _to.RemoveAt(i--);
                }
            }
            var newTo = (GameObject)EditorGUILayout.ObjectField("Include", null, typeof(GameObject), true);
            ;
            if (newTo)
            {
                for (int i = 0; i < Selection.gameObjects.Length; i++)
                {
                    _to = _to.Add(Selection.gameObjects[i]);
                }
            }

            if (GUILayout.Button("Retarget"))
            {
                Transform targetBone = _from.transform;
                for (int j = 0; j < _to.Length; j++)
                {
                    Transform changeBone = _to[j].transform;

                    foreach (var rend in changeBone.GetComponentsInChildren<SkinnedMeshRenderer>())
                    {
                        rend.rootBone = targetBone.FindChildDeep(rend.rootBone.name);
                        var bones = rend.bones;
                        for (int i = 0; i < bones.Length; i++)
                        {
                            bones[i] = targetBone.FindChildDeep(bones[i].name);
                        }
                        rend.bones = bones;

                        rend.transform.SetParent(targetBone, false);
                    }
                }
            }
        }
    }
}