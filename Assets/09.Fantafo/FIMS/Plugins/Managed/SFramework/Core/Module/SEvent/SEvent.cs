﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if ENABLE_SEVENT
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UObject = UnityEngine.Object;

public class SEvent : ScriptableObject
{
    public List<SDelegate> delegateList;

    public static SEvent operator +(SEvent lhs, SDelegate rhs)
    {
        if (lhs.delegateList == null) lhs.delegateList = new List<SDelegate>();
        lhs.delegateList.Add(rhs);
        return lhs;
    }

    public static SEvent operator +(SEvent lhs, Action rhs)
    {
        if (lhs.delegateList == null) lhs.delegateList = new List<SDelegate>();
        SDelegate del = ScriptableObject.CreateInstance<SDelegate>();
        del.SetCallback(rhs);
        lhs.delegateList.Add(del);
        return lhs;
    }
}
public static class SEventExtension
{
    public static bool Execute(this SEvent evnt)
    {
        if (evnt == null || evnt.delegateList == null || evnt.delegateList.Count == 0) return false;

        for (int i = 0; i < evnt.delegateList.Count; i++)
        {
            evnt.delegateList[i].Execute();
        }
        return true;
    }
}
#endif