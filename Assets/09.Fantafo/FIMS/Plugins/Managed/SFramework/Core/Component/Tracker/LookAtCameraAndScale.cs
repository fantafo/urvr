﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

[ExecuteInEditMode]
public class LookAtCameraAndScale : AbsTracker
{

    public Camera _target;
    public Vector3 _offset;
    public float _scale = 1;
    public float _minScale = 0;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null && Camera.main == null)
            return;
#endif
        Transform target = (_target != null ? _target.transform : Camera.main.transform);
        Vector3 direction = target.position - transform.position;
        transform.rotation = Quaternion.LookRotation(direction) * Quaternion.Euler(_offset);

        float scale = Mathf.Max((_scale * direction.magnitude), _minScale);
        transform.localScale = Vector3.one * scale;
    }

}