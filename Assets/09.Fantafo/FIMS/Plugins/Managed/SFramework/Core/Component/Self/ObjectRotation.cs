﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("SF/Self/Object Rotation")]
public class ObjectRotation : MonoBehaviour
{
    public Vector3 axis = Vector3.up;
    [Tooltip("1초당 회전량 (360도 기준)")]
    public float angle = 90;

    public bool resetOnEnable = true;
    Quaternion startupRotation = Quaternion.identity;

    private void Awake()
    {
        startupRotation = transform.rotation;
    }

    private void OnEnable()
    {
        if (resetOnEnable)
        {
            transform.rotation = startupRotation;
        }
    }

    private void LateUpdate()
    {
        transform.Rotate(axis, angle * Time.deltaTime, Space.Self);
    }
}
