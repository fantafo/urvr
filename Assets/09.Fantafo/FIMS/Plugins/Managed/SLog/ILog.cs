﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ILog
{
    string Tag { get; set; }
    void DebugFormat(string message, params object[] args);
    void Debug(string message);
    void Debug(string tag, string message);
    void WarnFormat(string message, params object[] args);
    void Warn(string message);
    void Warn(string tag, string message);
    void ErrorFormat(string message, params object[] args);
    void Error(string message);
    void Error(string tag, string message);
    void Exception(Exception e);
    void Exception(string tag, Exception e);
}
