﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// MainPlayer의 위치를 따라가게끔 하는 컴포넌트
    /// </summary>
    [ExecuteInEditMode]
    public class MainPlayerPositionTracker : AbsTracker
    {
        [Space]
        public MainPlayerController _target;
        public Vector3 localPosition;
        public Vector3 rotationOffset;
        Quaternion _rotationOffset;

        protected override void Start()
        {
            base.Start();
            _rotationOffset = Quaternion.Euler(rotationOffset);
        }
        private void OnValidate()
        {
            Start();
        }

        public override void OnReposition()
        {
            MainPlayerController tc = _target ?? MainPlayerController.main;
            if (tc == null)
                tc = GameObject.FindObjectOfType<MainPlayerController>();

            if (tc != null)
            {
                Transform target = tc.transform;
                transform.position = target.position + (target.rotation * localPosition);
                transform.rotation = target.rotation * _rotationOffset;
            }
        }
    }
}