﻿using System;
using System.Collections;
using System.IO;
using System.Threading;
using UnityEngine;
using FTF;

public class FimsAppController : SMonoBehaviour
{
    const string KEY = "launch.id";
    static string launchID;
    static Thread checkThread;
    int frameCount;

    ILog log = SLog.GetLogger(typeof(FimsAppController));

    void Awake()
    {
        log.Debug("Awake Alloc:"+FimsCommonData.IsAllocated);
        if (checkThread == null)
        {
            checkThread = new Thread(checkRunnable);
            checkThread.Name = "FimsCheck";
            checkThread.Priority = System.Threading.ThreadPriority.Lowest;
            checkThread.Start();
        }
    }

    private void Update()
    {
        if((Time.frameCount % 30) == 0)
        {
            frameCount = Time.frameCount;
            FimsCommonData.SetFlag(FimsCommonData.FLAG_LIVE, frameCount.ToString());
        }
    }

    DateTime clickTime;
    int clickCount = 0;
    private void OnGUI()
    {
        var pos = Event.current.mousePosition;
        var res = Screen.currentResolution;
        float btnsize = res.width * 0.1f;

        if (GUI.Button(new Rect(0, 0, btnsize, btnsize), "X", "Label"))
        {
            if((DateTime.Now - clickTime).TotalMilliseconds < 500)
            {
                if(++clickCount == 5)
                {
                    Debug.Log("Exit Side Click");
                    Application.Quit();
                }
            }
            else
            {
                clickCount = 1;
            }
            clickTime = DateTime.Now;
        }
    }

    private void OnDestroy()
    {
        checkThread = null;
    }

    void OnApplicationPause(bool pause)
    {
        log.Debug("OnPause(" + pause + ")");
        if (!pause)
        {
            CheckApplicationRun();
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        log.Debug("OnFocus(" + focus + ")");
        if (focus)
        {
            CheckApplicationRun();
        }
    }

    private void OnApplicationQuit()
    {
        log.Debug("Quit");
        OnDestroy();
    }

    bool CheckApplicationRun(bool useThread = false)
    {
        // Launch.ID가 등록됐는데도 Connect.prop이 사라졌다면 게임을 종료한다.
        if (launchID != null)
        {
            if (File.Exists(FimsCommonData.CONNECT_PATH))
            {
                Properties p = new Properties();
                p.Load(FimsCommonData.CONNECT_PATH);

                string currentID = p.GetString(KEY, null);
                if (currentID == launchID)
                {
                    //log.Debug("Same LaunchID : " + launchID);
                }
                else
                {
                    AppExiter.Exit("FimsAppController.Check.Quit - Different LaunchID");
                    return false;
                }
            }
            else
            {
                //if (useThread)
                //{
                //    // 쓰레드에서 직접호출한경우 1초동안 10회에걸쳐 재검사를 실시할때까지 커넥션 정보가 복구가 되지 않았다면 재실행한다.
                //    for(int i=0; i<10; i++)
                //    {
                //        if(CheckApplicationRun())
                //        {
                //            return true;
                //        }
                //        Thread.Sleep(100);
                //    }
                //}

                // 시작할 때 Launch.ID가 있었는데 중간에 없어졌다면
                // 상황이 달라진 것이기에 어플을 파기한다.
                AppExiter.Exit("FimsAppController.Check.Quit - Not found LaunchID");
                return false;
            }
        }

        // Alloc이 해제되면 어플을 종료한다.
        //if (!FimsCommonData.IsAllocated)
        //{
        //    AppExiter.Exit("FimsAppController.Check.Quit - Appeared 'Alloc' Flag");
        //}

        // Score가 등록됐을경우에도 게임을 종료한다.
        if (File.Exists(FimsCommonData.SCORE_PATH))
        {
            AppExiter.Exit("FimsAppController.Check.Quit - Appeared 'Score.prop'");
            return false;
        }
        return true;
    }

    public void checkRunnable()
    {
        try
        {
            // 컨넥션 정보가 있다면 런치아이디를 기록해둔다.
            if (File.Exists(FimsCommonData.CONNECT_PATH))
            {
                Properties p = new Properties();
                p.Load(FimsCommonData.CONNECT_PATH);
                launchID = p.GetString(KEY, null);
                log.Debug("Awake - LaunchID: " + launchID);
            }
            else
            {
                log.Debug("Awake - LaunchID is None");
            }

            // 스코어 정보가 이미 기록 돼 있다면 지운다.
            if (File.Exists(FimsCommonData.SCORE_PATH))
                File.Delete(FimsCommonData.SCORE_PATH);

            // 검사 시작
            int liveFrameCount = 0;
            while (checkThread != null)
            {
                try
                {
                    if (liveFrameCount++ % 10 == 0)
                    {
                        //log.Debug("Check");
                    }

                    if (CheckApplicationRun(true) == false)
                        break;

                    //  컨트롤러가 할당된 상태라면 클래그를 남긴다
                    if (VRInput.isPresentController)
                    {
                        FimsCommonData.SetFlag(FimsCommonData.FLAG_CONTROLLER, frameCount.ToString());
                    }/*
                    else
                    {
                        FimsCommonData.PopFlag(FimsCommonData.FLAG_CONTROLLER);
                    }*/

                    Thread.Sleep(500);
                }
                catch (Exception e)
                {
                    e.PrintStackTrace();
                }
            }
        }
        catch (Exception e)
        {
            e.PrintStackTrace();
        }
    }
}
