﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine.EventSystems;

namespace FTF
{
    /// <summary>
    /// GvrLaserPointer를 상속하여 컨트롤러 이벤트만 오버라이드한 클래스.
    /// 이것을 사용해야 다른 플렛폼에서도 켄버스 버튼을 클릭할 수 있습니다.
    /// CameraRig이나 컨트롤러에 직접 붙이는 것이 아니라, 다른 플렛폼에서도 공통으로 사용할수있도록 위치시킵니다. (MainPlayer prefab 참조)
    /// </summary>
    public class FTFLaserPointer : GvrLaserPointer
    {
        public override bool TriggerDown
        {
            get
            {
                return Input.GetMouseButtonDown(0) || VRInput.ClickButtonDown;
            }
        }
        public override bool Triggering
        {
            get
            {
                return Input.GetMouseButton(0) || VRInput.ClickButton;
            }
        }
        public override bool TriggerUp
        {
            get
            {
                return Input.GetMouseButtonUp(0) || VRInput.ClickButtonUp;
            }
        }
        public override bool TouchDown
        {
            get
            {
                return VRInput.TouchDown;
            }
        }
        public override bool IsTouching
        {
            get
            {
                return VRInput.IsTouching;
            }
        }
        public override bool TouchUp
        {
            get
            {
                return VRInput.TouchUp;
            }
        }
        public override Vector2 TouchPos
        {
            get
            {
                return VRInput.TouchPos;
            }
        }
    }
}