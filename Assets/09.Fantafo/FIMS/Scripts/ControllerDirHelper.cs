﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 컨트롤러 방향에 따라서 엑션을 달리 하기위해 만들었습니다.
/// 이것을 참조해서 컨트롤러가 정방향으로 들고있는지, 뒤집고있는지 등에 관한 간단한 Direction을 획득할 수 있습니다.
/// 
/// 하지만, 현재 이것을 Earth에서 이용하지 않습니다.
/// TODO : (내부 FIMS 코드에서는 연계가 돼 있습니다. 이게 필요가 없다면 제거하는게 옳습니다.)
/// </summary>
public class ControllerDirHelper : SMonoBehaviour
{
    public static ControllerDirHelper main;
    public static bool isUp = true;
    public static bool IsLeft;
    public static bool IsRight;
    public static bool isDown;

    public Vector3 upDirection;
    public bool _isUp = true;
    public bool _IsLeft;
    public bool _IsRight;
    public bool _isDown;

    float[] dirs = new float[6];

    private Camera _mainCamera;
    private Transform _mainCameraTr;

    private void Awake()
    {
        main = this;
    }

    private void Start()
    {
        _mainCamera = Camera.main;
        _mainCameraTr = _mainCamera.transform;
    }

    private void Update()
    {
        //upDirection = (Camera.main.transform.rotation.Inverse() * GameObject.Find("ControllerVisual").transform.rotation) * Vector3.up;
        upDirection = (_mainCameraTr.rotation.Inverse() * VRInput.controllerOrientation) * Vector3.up;

        dirs[0] = (upDirection - Vector3.up).sqrMagnitude;
        dirs[1] = (upDirection - Vector3.left).sqrMagnitude;
        dirs[2] = (upDirection - Vector3.right).sqrMagnitude;
        dirs[3] = (upDirection - Vector3.down).sqrMagnitude;
        dirs[4] = (upDirection - Vector3.forward).sqrMagnitude;
        dirs[5] = (upDirection - Vector3.back).sqrMagnitude;

        //_isUp = isUp = (dirRank(0) == 5) || (dirRank(4) == 5) || (dirRank(5) == 5);
        //_IsLeft = IsLeft = (dirRank(1) == 5);
        //_IsRight = IsRight = (dirRank(2) == 5);
        _isDown = isDown = (dirRank(3) == 5);
        _isUp = isUp = !isDown;
    }
    int dirRank(int idx)
    {
        int rank = dirs.Length;
        for (int i = 0; i < dirs.Length; i++)
        {
            if (dirs[i] <= dirs[idx])
            {
                rank--;
            }
        }
        return rank;
    }
}
