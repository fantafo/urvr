﻿using FTF.Packet;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    /// <summary>
    /// 씬을 ASync로 읽어올때 사용하는 abstract클래스다.
    /// 일반적으로 Room의 씬을 읽어올때 사용한다.
    /// </summary>
    public abstract class LoadBase : SMonoBehaviour
    {
        bool fadeComplete;
        bool isLoading;

        protected abstract bool LoadValidate();
        protected abstract AsyncOperation LoadAsync();
        protected virtual void LoadCompelte() { }

        protected virtual void LoadStart()
        {
            if (isLoading)
                return;
            isLoading = true;

            if (LoadValidate())
            {
                Debug.Log(GetType() + "/ LoadValidated!");
                if (MainPlayerController.main != null)
                {
                    Debug.Log(GetType() + "/ Exist MainPlayerController");
                    fadeComplete = false;
                    ScreenFader.main.FadeOut(() =>
                    {
                        fadeComplete = true;
                    });
                }
                else
                {
                    fadeComplete = true;
                }
                StartCoroutine(onLoadStart());
            }
        }
        IEnumerator onLoadStart()
        {
            yield return null;
            Debug.Log(GetType() + "/ LoadStart");
            var op = LoadAsync();

            op.allowSceneActivation = false;

            while (!fadeComplete)
            {
                yield return null;
            }

            op.allowSceneActivation = true;
            AbstractPacketHandler.ReadablePacket = false;
            yield return op;
            yield return null;
            AbstractPacketHandler.ReadablePacket = true;

            Debug.Log(GetType() + "/ LoadComplete");
            Networker.Send(C_Room.LoadComplete());
            Networker.State = NetworkState.Playing;
            LoadCompelte();
            isLoading = false;
        }
    }
}