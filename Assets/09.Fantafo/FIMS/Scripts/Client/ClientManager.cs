﻿#if UNITY_EDITOR
#define OFFLINE
#define OBSERVER
#define CLIENT
#endif

#if FTF_OFFLINE
#define OFFLINE
#elif FTF_OBSERVER
#define OBSERVER
#else
#define CLIENT
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    /// <summary>
    /// 클라이언트가 잘 돌아갈 수 있게 만들어주는게 목적이다.
    /// 실제로는 에디터상에서 네트워크 매니저를 설치하는데 사용된다.
    /// </summary>
    public class ClientManager : SMonoBehaviour
    {
        public static ClientManager main;


        public GameObject[] dependencies;

#if CLIENT
        public GameObject[] client;
#endif
#if OBSERVER
        public GameObject[] observer;
#endif
#if OFFLINE
        public GameObject[] offline;
#endif

        private void Reset()
        {
            Debug.Log("Reset");
        }

        private void Awake()
        {
            FimsCommonData.PopFlag(FimsCommonData.FLAG_LOADING);

            Debug.Log("= = = = = LOAD NEW SCENE = = = = =");
            if (main)
            {
                gameObject.SetActive(false);
                gameObject.Destroy();
                return;
            }


            main = this;
            transform.SetParent(null);
            DontDestroyOnLoad(gameObject);

            Instantiates(dependencies, transform);
#if FTF_OFFLINE
            Debug.Log("ClientManager-Offline");
            Instantiates(offline, null);
#elif FTF_OBSERVER
            Debug.Log("ClientManager-Observer");
            Instantiates(observer, null);
#else 
            Debug.Log("ClientManager-Client");
            Instantiates(client, null);
#endif
        }

        private void Instantiates(GameObject[] objs, Transform parent = null)
        {
            if (objs != null)
            {
                foreach (var go in objs)
                {
                    Instantiate(go, parent);
                }
            }
        }
    }
}
