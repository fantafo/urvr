﻿using FTF.Packet;
using System.Collections;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 클라이언트 본인에 관련된 VR 트레킹 정보를 서버에 전송합니다.
    /// </summary>
    public class MainPlayerTrackingSender : SMonoBehaviour
    {
        public float delay = 0.05f;

        private void OnEnable()
        {
            StartCoroutine(SendData());
        }

        private IEnumerator SendData()
        {
            WaitForSeconds wait = new WaitForSeconds(delay);
            while (true)
            {
                if (Networker.State >= NetworkState.Room)
                {

                    Networker.Send(C_Common.TrackingPosition(
                        VRInput.headPosition,
                        VRInput.headOrientation,
                        VRInput.isPresentController,
                        VRInput.controllerPosition,
                        VRInput.controllerOrientation,
                        (PlayerInstance.lookMain.IsRoomMaster || VRInput.ClickButton),
                        FTFReticlePosition.Value));
                }

                yield return wait;
            }
        }
    }
}