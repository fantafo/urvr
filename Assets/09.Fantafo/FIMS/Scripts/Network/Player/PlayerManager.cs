﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// PlayerInstance(유저) 정보를 관리한다.
    /// 기존에 등록된 유저나, instanceID만 가지고 있는 유저의 정보를 가져올 수 있다.
    /// </summary>
    public class PlayerManager
    {
        class RequestPlayerInstance
        {
            public int instanceID;
            public Action<PlayerInstance> callback;
        }

        private static object LOCK = new object();
        private static Dictionary<int, RequestPlayerInstance> s_requested = new Dictionary<int, RequestPlayerInstance>();
        private static Dictionary<int, PlayerInstance> s_players = new Dictionary<int, PlayerInstance>();
        public static PlayerInstance MasterPlayer;
        /// <summary>
        /// 유저의 정보를 가져온다.
        /// 이전에 가지고있던 정보의 경우에는 바로 return으로 반환하며 갖고있지 않은 정보는 callback을 통해 반환해준다.
        /// </summary>
        public static PlayerInstance GetPlayer(int instanceID, Action<PlayerInstance> callback = null)
        {
            lock (LOCK)
            {
                PlayerInstance player;
                if (s_players.TryGetValue(instanceID, out player))
                {
                    if (callback != null)
                        callback(player);
                    return player;
                }

                if (callback != null)
                {
                    RequestPlayerInstance rpi;
                    if (s_requested.TryGetValue(instanceID, out rpi))
                    {
                        rpi.callback += callback;
                        return null;
                    }

                    s_requested.Add(instanceID, new RequestPlayerInstance { instanceID = instanceID, callback = callback });
                    Networker.Send(C_Common.RequestUserInfo(instanceID));
                }
                return null;
            }
        }

        /// <summary>
        /// 불러오거나 임의로 추가한 플레이어를 목록에 추가한다.
        /// </summary>
        public static void AddPlayer(PlayerInstance player)
        {
            if (player.IsRoomMaster)
            {
                MasterPlayer = player;
            }
            lock (LOCK)
            {
                s_players.Add(player.instanceId, player);

                RequestPlayerInstance rpi;
                if (s_requested.TryGetValue(player.instanceId, out rpi))
                {
                    rpi.callback(player);
                }

                if (PlayerInstance.main == null)
                {
                    PlayerInstance.main = player;
                    PlayerInstance.lookMain = player;
                }
            }
        }

        /// <summary>
        /// 새로운 플레이러를 발견하여 생성할 때 사용한다.
        /// </summary>
        public static PlayerInstance NewPlayer(int instanceID)
        {
            return new PlayerInstance { instanceId = instanceID };
        }

        /// <summary>
        /// 모든 플레이어 정보를 삭제한다.
        /// </summary>
        public static void Clear()
        {
            s_players.Clear();
            s_requested.Clear();
        }

    }
}