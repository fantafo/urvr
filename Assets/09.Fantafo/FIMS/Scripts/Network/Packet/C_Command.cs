﻿using System;
using System.Collections.Generic;
using fCommand = FTF.Command;

namespace FTF.Packet
{
    /// <summary>
    /// 플레이어끼리 소통하기 위한 커맨드 패킷 모음
    /// </summary>
    public class C_Command
    {
        /// <summary>
        /// 에디터 상에서만 검사한다.
        /// </summary>
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        static void Check(IList<fCommand> commands, IList<PlayerInstance> targets)
        {
            // 옵코드1개, 패킷갯수1, 유저갯수1, 유저ID(255 * 4)
            const int COMMANDS_ALLOWSIZE = 0xFFFF - (BasePacketExtension.OPCODE_SIZE + 1 + 1 + (255 * 4));

            if (commands != null)
            {
                if (commands.Count > 255)
                    throw new ArgumentOutOfRangeException("Command 갯수가 255개를 넘을 수 없습니다.");
                int totalBytes = 0;
                for (int i = 0; i < commands.Count; i++)
                    totalBytes += commands[i].Length;

                if (totalBytes > COMMANDS_ALLOWSIZE)
                    throw new ArgumentOutOfRangeException("전체 패킷량이 너무 많습니다. 패킷을 나누거 줄여주세요.");
            }

            if (targets != null && targets.Count > 255)
                throw new ArgumentOutOfRangeException("유저 수가 255명을 넘을 수 없습니다.");
        }

        /// <summary>
        /// 특정 유저에게 보낸다.
        /// </summary>
        public static byte[] Command(byte[] command, PlayerInstance target)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command, 0, command.Length);

            w.WriteC(1);
            w.WriteD(target.instanceId);
            return w.ToBytes();
        }

        /// <summary>
        /// 특정 유저들에게 보낸다.
        /// </summary>
        public static byte[] Command(byte[] command, IList<PlayerInstance> targets)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command, 0, command.Length);

            w.WriteC(targets.Count);
            if (targets.Count < 255) // 255명이 대상일 경우 BroadcastAll 대상
                for (int i = 0; i < targets.Count; i++)
                {
                    w.WriteD(targets[i].instanceId);
                }
            return w.ToBytes();
        }

        /// <summary>
        /// 특정 유저에게 보낸다.
        /// </summary>
        public static byte[] Command(fCommand command, PlayerInstance target)
        {
            Check(new fCommand[] { command }, new PlayerInstance[] { target });

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command.Buffer, 0, command.Length);

            w.WriteC(1);
            w.WriteD(target.instanceId);
            return w.ToBytes();
        }

        /// <summary>
        /// 특정 유저들에게 보낸다.
        /// </summary>
        public static byte[] Command(fCommand command, IList<PlayerInstance> targets)
        {
            Check(new fCommand[] { command }, targets);

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command.Buffer, 0, command.Length);

            w.WriteC(targets.Count);
            if (targets.Count < 255) // 255명이 대상일 경우 BroadcastAll 대상
                for (int i = 0; i < targets.Count; i++)
                {
                    w.WriteD(targets[i].instanceId);
                }
            return w.ToBytes();
        }

        /// <summary>
        /// 나를 제외한 파티유저에게 보낸다.
        /// </summary>
        public static byte[] Broadcast(byte[] command)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command, 0, command.Length);

            w.WriteC(byte.MinValue);//Broadcast

            return w.ToBytes();

        }

        /// <summary>
        /// 나를 제외한 파티유저에게 보낸다.
        /// </summary>
        public static byte[] Broadcast(fCommand command)
        {
            Check(new fCommand[] { command }, null);

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command.Buffer, 0, command.Length);

            w.WriteC(byte.MinValue);//Broadcast

            return w.ToBytes();

        }
        /// <summary>
        /// 나를 포함해서 파티유저에게 보낸다.
        /// </summary>
        public static byte[] BroadcastAll(byte[] command)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command, 0, command.Length);

            w.WriteC(byte.MaxValue);//Broadcast All

            return w.ToBytes();
        }
        /// <summary>
        /// 나를 포함해서 파티유저에게 보낸다.
        /// </summary>
        public static byte[] BroadcastAll(fCommand command)
        {
            Check(new fCommand[] { command }, null);

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(1);
            w.WriteH(command.Length);
            w.Write(command.Buffer, 0, command.Length);

            w.WriteC(byte.MaxValue);//Broadcast All

            return w.ToBytes();
        }

        /// <summary>
        /// 특정 유저에게 커맨드를 여러개 보낸다.
        /// </summary>
        public static byte[] Command(IList<fCommand> commands, PlayerInstance target)
        {
            Check(commands, new PlayerInstance[] { target });

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(commands.Count);
            for (int i = 0; i < commands.Count; i++)
            {
                w.WriteH(commands[i].Length);
                w.Write(commands[i].Buffer, 0, commands[i].Length);
            }

            w.WriteC(1);
            w.WriteD(target.instanceId);
            return w.ToBytes();
        }


        /// <summary>
        /// 특정 유저득에게 커맨드를 여러개 보낸다.
        /// </summary>
        public static byte[] Command(IList<fCommand> commands, IList<PlayerInstance> targets)
        {
            Check(commands, targets);

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(commands.Count);
            for (int i = 0; i < commands.Count; i++)
            {
                w.WriteH(commands[i].Length);
                w.Write(commands[i].Buffer, 0, commands[i].Length);
            }

            w.WriteC(targets.Count);
            if (targets.Count < 255) // 255명이 대상일 경우 BroadcastAll 대상
                for (int i = 0; i < targets.Count; i++)
                {
                    w.WriteD(targets[i].instanceId);
                }
            return w.ToBytes();
        }

        /// <summary>
        /// 나를 제외한 파티유저에게 보낸다.
        /// </summary>
        public static byte[] Broadcast(IList<fCommand> commands)
        {
            Check(commands, null);

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(commands.Count);
            for (int i = 0; i < commands.Count; i++)
            {
                w.WriteH(commands[i].Length);
                w.Write(commands[i].Buffer, 0, commands[i].Length);
            }

            w.WriteC(byte.MinValue);//Broadcast

            return w.ToBytes();

        }
        /// <summary>
        /// 나를 포함해서 파티유저에게 보낸다.
        /// </summary>
        public static byte[] BroadcastAll(IList<fCommand> commands)
        {
            Check(commands, null);

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.C_Command);
            w.WriteC(commands.Count);
            for (int i = 0; i < commands.Count; i++)
            {
                w.WriteH(commands[i].Length);
                w.Write(commands[i].Buffer, 0, commands[i].Length);
            }

            w.WriteC(byte.MaxValue);//Broadcast All

            return w.ToBytes();
        }
    }
}