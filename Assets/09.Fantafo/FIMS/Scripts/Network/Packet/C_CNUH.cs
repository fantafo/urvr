﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF.Packet
{
    class C_CNUH
    {
        public static byte[] Start()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.CNUH_Ready);
            return w.ToBytes();
        }
    }
}
