﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FTF;
using FTF.Packet;
using System.IO;
using RenderHeads.Media.AVProVideo;     // 360 비디오 제어용


public class startMedia : AutoCommander
{

    public struct videoStruct
    {
        public string name;
        public int videoType;
        /*videoType == 0 (180도 영상 3d)
         *videoType == 1 (360도 영상 2d) 
         *videoType == 2 (360도 영상 3d)
         */
        public videoStruct(string name, int type)
        {
            this.name = name;
            this.videoType = type;
        }

    }



    float time;
    public MediaPlayer video;



    private const string _filePath = "/storage/emulated/0/Fantafo/mp4/";
    private const string _filePathForPC = "I:/1.Build/FimsFiileServer";
    private const string _filePathForOBSERVER = "360VRVideo/";
    string path = string.Empty;

    public GameObject Sphere_3D_360;
    public GameObject Sphere_2D_360;
    public GameObject Sphere_3D_180;

    public Coroutine startSender;
    public Coroutine SyncCor;

    private Dictionary<int, videoStruct> videoDitionary;


    public Dropdown tmp;

    void Start()
    {
        videoDitionary = new Dictionary<int, videoStruct>();



        if (Application.platform == RuntimePlatform.Android)
        {
            path = _filePath;
        }
        else
        {
            path = _filePathForPC;
        }
#if FTF_OBSERVER
        string tempDirectory = null;
        if (File.Exists("videopathinfo.txt"))
        {
            tempDirectory = File.ReadAllText("videopathinfo.txt");
        }
        try
        {
            path = tempDirectory;
        }
        catch
        {
        }
#endif

        startVideo(RoomInfo.main.fileName);
        startSender = StartCoroutine(startSend());
        SyncCor = StartCoroutine(SyncVideo());

#if FTF_OBSERVER
#endif
    }

    IEnumerator SyncVideo()
    {
        while (true)
        {
            if (Mathf.Abs(video.Control.GetCurrentTimeMs() - RoomInfo.main.videoMs) > 3000)
            {
                video.Control.SeekFast(RoomInfo.main.videoMs);
            }

            yield return new WaitForSeconds(5.0f);
        }
    }

    [OnCommand]
    public void VideoSync(float currentMs)
    {
#if FTF_OBSERVER
        if (Mathf.Abs(video.Control.GetCurrentTimeMs() - currentMs) > 3000)
        {
            video.Control.SeekFast(currentMs);
        }
#endif

    }


    IEnumerator startSend()
    {
        while (!video.startReady)
        {
            yield return new WaitForSeconds(0.5f);
        }
        Networker.Send(C_CNUH.Start());
    }
    // Update is called once per frame

    /*void Update ()
    {
      

    }*/

    //public void govideo()
    //{
    //    startVideo(tmp.value - 1);
    //    //Networker.Send(C_CNUH.Start());
    //    video.Play();
    //}
    void DirFileSearch(string path, string file)
    {
        string filePath = null;
        Debug.Log("Path : " + path);

        string[] dirs = Directory.GetDirectories(path);
        string[] files = Directory.GetFiles(path, file);



        for (int i = 0; i < dirs.Length; i++)
        {
            Debug.Log(dirs[i]);
        }

        for (int i = 0; i < files.Length; i++)
        {
            Debug.Log(files[i]);
        }
        foreach (string f in files)
        {
            this.path = f;
        }
        if (dirs.Length > 0)
        {
            foreach (string dir in dirs)
            {
                DirFileSearch(dir, file);
            }
        }

    }





    void startVideo(string fileName)
    {
        Debug.Log("Start Video Message Received " + fileName);

        video.Stop();
        //videoStruct tmp = new videoStruct();
        //Debug.Log("tmp : " + tmp);
#if FTF_OBSERVER
        string pathCopy = path;
        Debug.Log("pathCopy : " + pathCopy);
        DirFileSearch(pathCopy, fileName);
        video.m_VideoPath = path;
        if (pathCopy == path)
        {
            Debug.Log("file not found!");
        }
        else
        {
            Debug.Log("file Found" + path);
        }
#else
        video.m_VideoPath = path + fileName;
#endif



        video.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, video.m_VideoPath, false);

        Debug.Log("FilePaht : " + video.m_VideoPath);


        Sphere_3D_360.SetActive(false);
        Sphere_2D_360.SetActive(true);
        Sphere_3D_180.SetActive(false);
        //if (videoDitionary.TryGetValue(num, out tmp))
        //{
        //    video.m_VideoPath = path + tmp.name;
        //    video.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, video.m_VideoPath, false);

        //    if (tmp.videoType == 0)
        //    {
        //        Sphere_3D_360.SetActive(false);
        //        Sphere_2D_360.SetActive(false);
        //        Sphere_3D_180.SetActive(true);
        //    }
        //    else if (tmp.videoType == 1)
        //    {
        //        Sphere_3D_360.SetActive(false);
        //        Sphere_2D_360.SetActive(true);
        //        Sphere_3D_180.SetActive(false);
        //    }
        //    else
        //    {
        //        Sphere_3D_360.SetActive(true);
        //        Sphere_2D_360.SetActive(false);
        //        Sphere_3D_180.SetActive(false);
        //    }
        //}
#if FTF_OBSERVER
        video.Play();
#endif
        //video.Play();
    }

}
